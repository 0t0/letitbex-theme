[![Gem Version](https://badge.fury.io/rb/letitbex-theme.png)](https://badge.fury.io/rb/letitbex-theme)

## Installisation
- Add this line to Gemfile:
 > gem "letitbex-theme"
- Fetch and update bundled gems by running the following Bundler command:
 > bundle
- Set theme and style in _config.yml:
```
  theme: letitbex-theme
  style: hacker 
```
 hacker is the default style. The available styles are listed below

## Usage
###  Addition configs to config.yml
- `header_pages`: to specify which pages should be displayed in navbar
- `footer`: string, which will be inserted on the end of the page (doesn't support markup, but html)
- To set bacground image to copy image to assets folder as 'bg.png'. 'bg.png' is the default value, in case  you want to change the name or use image with different extension change the value in 'assets/base.scss' line 22 in gems folder where theme is present. 
- Run 'add-theme.sh' to create themes from image. The script requires [pywal](https://pypi.org/project/pywal/) colorschemes to be present. 

## Styles
### d1
![d1](d1.gif)

### dune
![dune](dune.gif)

### gits
![gits](gits.gif)

### hacker
![hacker](hacker.gif)

### leaves
![leaves](leaves.gif)

### neon
![neon](neon.gif)

### terrain
![terrain](terrain.gif)
