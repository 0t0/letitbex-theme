# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "letitbex-theme"
  spec.version       = "0.2.0"
  spec.authors       = ["mrtsukim0t0-"]
  spec.email         = ["mrtsukim0t0@tutanota.com"]

  spec.summary       = "Personalised copy of jekyll-theme-console"
  spec.homepage      = "https://gitlab.com/0t0/letitbex-theme.git"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 3.5"
  spec.add_runtime_dependency "jekyll-seo-tag"
  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
end
